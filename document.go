package documents

import "bitbucket.org/matchmove/risk-interests"

// Document represent document interface
type Document interface {
	Parse()
	Format() interests.Interests
}

// DocumentObject represent document interface
type DocumentObject struct {
	Document
	Body        string
	Individuals []string
	Entities    []string
}
