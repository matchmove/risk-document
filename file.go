package documents

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

//Source constants
const (
	SourceUNOrg    = "www.un.org"
	SourceGovUK    = "www.gov.uk"
	SourceEuropaEU = "ec.europa.eu"
)

const (
	// FileStatusInitial initial status value for `file` record
	FileStatusInitial = 0
	// FileStatusDownloaded status for downloaded file for `file` record
	FileStatusDownloaded = 1
	// FileStatusConverted status for converting pdf to text
	FileStatusConverted = 2
	// FileStatusProcessed status for process and uploaded for `file` record
	FileStatusProcessed = 3
	// FileTypePDF file type pdf
	FileTypePDF = "pdf"

	// FileTypeXML file type xml
	FileTypeXML = "xml"

	// FileTypeText file type txt
	FileTypeText = "txt"
)

// File represent the file to be process
type File struct {
	ID       string `json:"-"`
	Filename string `json:"filename"`
	Path     string `json:"path"`
	FileType string `json:"file_type"`
	Status   string `json:"status"`
	Contents string `json:"-"`
}

// Files represent and array of file
type Files []File

// GetLatestFiles returns the file to be loaded as a configuration source
func GetLatestFiles(source string) (Files, error) {
	defer func() {
		if recovery := recover(); recovery != nil {
			log.Println("Recovering from GetLatestFiles Exception:", recovery)
		}
	}()

	var files Files

	sourceDir, _ := ioutil.ReadDir(source)
	for _, item := range sourceDir {

		if strings.ToLower(item.Name()) == ".ds_store" {
			continue
		}

		ext := filepath.Ext(item.Name())
		file := File{
			Filename: item.Name(),
			Path:     source,
			FileType: strings.ToLower(ext[1:]),
		}

		if file.FileType == "pdf" {
			file.PDFToText()
			file.FileType = FileTypeText
		}

		if !strings.Contains("txt|xml|pdf", strings.ToLower(ext[1:])) {
			continue
		}

		files = append(files, file)
	}

	return files, nil
}

// ReadFile reads the file and return the contents
func (f *File) ReadFile() error {
	log.Println("Opening ReadFile:", f.Path+f.Filename)
	document, err := os.Open(f.Path + f.Filename)
	if err != nil {
		log.Printf("Failed Open: %s", err)
		return err
	}

	body, err := ioutil.ReadAll(document)
	if err != nil {
		log.Printf("Failed ReadAll: %s", err)
		return err
	}

	f.Contents = string(body)
	return nil
}

// PDFToText converts pdf to text
func (f *File) PDFToText() error {
	filename := f.Filename[0:strings.LastIndex(f.Filename, filepath.Ext(f.Filename))] + ".txt"
	cmd := "pdftotext"
	args := []string{"-layout", f.Path + f.Filename, f.Path + filename}
	if err := exec.Command(cmd, args...).Run(); err != nil {
		return err
	}
	os.Remove(f.Path + f.Filename)
	f.Filename = filename
	return nil
}

// Process parses the file and return the formatted response
func (f File) Process() error {
	err := f.ReadFile()
	if err != nil {
		return err
	}

	if strings.TrimSpace(f.Contents) == "" {
		return errors.New("Nothing to parse")
	}

	return nil
}

// Update update the file and moves them to a new destination
func (f *File) Update(destination string) error {
	os.Rename(f.Path+f.Filename, destination+f.Filename)
	f.Delete()
	return nil
}

// Delete delete the file
func (f *File) Delete() error {
	err := os.Remove(f.Path + f.Filename)
	if err != nil {
		return err
	}
	return nil
}
