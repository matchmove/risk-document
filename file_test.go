package documents

import (
	"log"
	"testing"
)

const (
	ProcessorSource = "documents/source/"
)

var createdIDs []string

func TestFilesGetLatest(t *testing.T) {
	files, err := GetLatestFiles(ProcessorSource)
	if err != nil {
		t.Errorf("Exception occured in files.GetLatest: %s", err)
	}

	for _, file := range files {
		log.Println(file.Filename, file.FileType)
	}
}

func TestFileUpdate(t *testing.T) {
	var file File
	err := file.Update(ProcessorSource)

	if err != nil {
		t.Errorf("Exception occured in file.Update: %s", err)
	}
}
